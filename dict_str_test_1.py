#!/usr/bin/env python

import datetime as dt
import math
import time

# maps short easy to read names to long serial numbers.
nameMap = {
  'T1':  '000006ad1e97',
  'T2':  '000006ad7869',
  'time':  'time'

}

# QUICK FUNCTION TO GENERATE SOME JUNK NUMBERS.
def generate_random_sensor_data():
  time = dt.datetime.now()
  d = {}
  d['time'] = time
  d['000006ad1e97'] = math.radians(time.second)
  d['000006ad7869'] = math.sin(math.radians(time.second))
  d['T3'] = math.cos(math.radians(time.second))

  return d

# Open the file for writing (will over-write an existing) and add the header 
with open("logfile.txt", 'w') as f:
  f.write("{:<25s}{:>7s}{:>7s}\n".format(
		'Date Time',
		'T1',
		'T2'
      ))

ctr = 0
while ctr < 5:
  ctr += 1
  print "Generating random data..."
  sd = generate_random_sensor_data()

  print "Writing (appending) to file..."
  with open("logfile.txt", 'a') as f:
    f.write("{:<25s}{:7.2f}{:7.2f}\n".format(
        sd['time'].strftime('%c'),
        sd[nameMap['T1']],  # <-- Use the nameMap to lookup the serial number!
        sd[nameMap['T2']],  #     serial number is returned, and used to lookup
                            #     data value in the sd dictionary!
    ))

  print "Resting..."
  time.sleep(1)

print "DONE."

#  Noted.   {:<30s}    left justifies the string in a 30 character space. Use for header and data
#   		{:>10s}{:>10s}    right justifies the strings in a 10 char wide space
#           :10.2f}{:10.2f}\n   right justify ( default for numbers) in 10 wide space.  float with 2 right of dec point