#!/usr/bin/python
import Adafruit_GPIO as GPIO
import Adafruit_GPIO.MCP230xx


# Use busnum = 0 for older Raspberry Pi's (256MB)
#mcp = home/pi/datacol-env/lib/python2.7/site-packages/Adafruit_GPIO/MCP230xx(busnum = 1, address = 0x20, num_gpios = 16)

#------------
#OR 
import Adafruit_GPIO.MCP230xx as MCP
mcp = MCP.MCP23017()
#--------------
# Use busnum = 1 for new Raspberry Pi's (512MB with mounting holes)
# mcp = Adafruit_MCP230XX(busnum = 1, address = 0x20, num_gpios = 16)

# Set pins 1  to 16 TO INPUT  (you can set pins 0..15 this way)
# this does not work - complains that GPIO is not defined
#mcp.setup_pins({ 1: 'GPIO.IN' , 2: 'GPIO.IN' , 3: 'GPIO.IN' , 4:'GPIO.IN'  , 4: 'GPIO.IN' , 6: 'GPIO.IN' , 7: 'GPIO.IN' , 8: 'GPIO.IN' })
#mcp.setup_pins({ 1: GPIO.OUT , 2: GPIO.OUT })
# Set pins to input with the pullup resistor enabled
#mcp.pullup({ 1:GPIO.True , 2:GPIO.IN , 3:GPIO.IN , 4:GPIO.IN , 4:GPIO.IN , 6:GPIO.IN , 7:GPIO.IN , 8:GPIO.IN })
for i in range (0,16):
	mcp.setup(i, GPIO.IN)


for i in range (0,16):
	mcp.pullup(i , True)
    
# Read pins 1, 2, 3 and display the results
mcp.input(1)
mcp.input(2)
mcp.input(3)

for i in range (0,16):
	print ('%d : %d' %(i+1 , mcp.input(i)))


