#!/usr/bin/env python

import datetime as dt
import math
import time
import os
import glob
# Import the ADS1x15 module.
import Adafruit_ADS1x15
from w1thermsensor import W1ThermSensor, SensorNotReadyError, NoSensorFoundError
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')		#to uninstall the kernel modules:  modprobe -r w1-therm w1-gpio

# Create an ADS1115 ADC (16-bit) instance.
adc = Adafruit_ADS1x15.ADS1115()
GAIN = 1
 
nameMap = { 'T1': '000006ae1e97' , 'T2': '000006ae7869', }	
  
def getFlowRate():
	value = adc.read_adc(0, gain=GAIN)  #  flow is ADC 0 
	#print value
	out = 5 *(value / 3267)
	flow =  (out - 0.388)/ 0.0778       #  flow in liters / sec
	flow = (flow-0.8) * 14.8			# adjustment to read correctly at 0 and 40 l/s
	if flow <= 0:
		flow = 0 
	return flow  

def getFlowTemp():
	value = adc.read_adc(1, gain=GAIN)  #  Temp is ADC 1	
	#print value
	out = 5 *(value / 3267)
	temp = 32 + (out*60)
	if out <= .5: 
		temp = 32
	return temp
 
def getKW():	
# 10 KW Boiler is modulated by cycling ON/OFF. avg kw = 10 * on time / (on time + off time )
# run a process in the loop which creates an list of the boiler state for each of last 10 sec
# 10 values. push and pop  one every second.  To read: KW = SUM of elements 
#  might have to adjust the timing to reflect the cycle time of the boiler.  - should be some paper notes somewhere 
#	for now  return 0 
	kw = 0 

	return kw

def getTemp(name):
	try:
		sensor = W1ThermSensor(W1ThermSensor.THERM_SENSOR_DS18B20, nameMap[name])
		temp = sensor.get_temperature()
		#print temp
	except SensorNotReadyError:
		#print("Sensor Not Ready")
		temp = 888
	except NoSensorFoundError:
		#print("Sensor Not Found")
		temp = 999
	return temp
	

 
 
header = ("{:>7s}{:>7s}{:>10s}{:>10s}".format( 'T1' , 'T2' , 'FlowRate' , 'FlowTemp'))
print header

data = ("{:7.1f}{:7.1f}{:10.1f}{:10.1f}\n".format(getTemp('T1') , getTemp('T2') , getFlowRate() , getFlowTemp() ))
print data
