#!/usr/bin/env python

'''
Feb 2019
A test script for experimenting with processing command line arguments
'''
import sys

print sys.argv

DEBUG = 0

def log(s):
	print DEBUG
	if DEBUG:
		print s
	else:
		pass
		

args = sys.argv
if len(args) > 1:
	if any([x in args for x in ['-h', '--help', 'H', 'help', 'HELP']]):
		print "HELP"
		sys.exit()
	if args[1] == 'debug':
		print "%%%%% DEBUG MODE %%%%%%"
		DEBUG = 1

log("some string")
