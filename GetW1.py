#!/usr/bin/env python

import datetime as dt
import math
import time

import os
import glob

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')		#to uninstall the kernel modules:  modprobe -r w1-therm w1-gpio

from w1thermsensor import W1ThermSensor, Sensor, SensorNotReadyError, NoSensorFoundError
#Two sensors available  000006ae1e97  and  000006ae7869 
 
for sensor in W1ThermSensor.get_available_sensors():
	print (sensor)

	try:
		print("Sensor %s has temperature %.2f" % (sensor.id, sensor.get_temperature()))
	except SensorNotReadyError:
		print("Sensor %s is not ready to read"  % (sensor.id))
	except NoSensorFoundError:
		print ("No Sensor %s Found")
		
	
#Single sensor works now and handles missing sensor  
#
"""
# get one sensor explicit
# these two are connected to a test board 
#un-comment as needed

#sensor_ID = "000006ae7869"
sensor_ID = "000006ae1e97"

try:
	sensor = W1ThermSensor(sensor_type=Sensor.DS18B20, sensor_id="sensorID")
	print( sensor.get_temperature())
except SensorNotReadyError:
	print("Sensor Not Ready")
#	print 
#	print("Sensor %s is not ready to read"  % (sensor.id))
except NoSensorFoundError:
	print("Sensor Not Found")
#	except SensorNotReadyError:
#		print("Sensor Not Ready")

"""