#!/usr/bin/env python

import pandas as pd
import numpy as np
from bokeh.plotting import figure, output_file, show, curdoc
from bokeh.layouts import gridplot, column, widgetbox
from bokeh.models.widgets import TextInput, Button, Paragraph, Dropdown
from bokeh.io import output_file, show
import os
import time
from time import strftime
import datetime as dt
from datetime import datetime, timedelta
import calendar

filename = '2017-11-05'
print filename
full_path_variable = os.path.join("/media/data1", "{:}.txt".format(filename))
df = pd.read_csv(full_path_variable , delim_whitespace=True)
len = len(df)
s = pd.Series(np.random.randint(0, 50, size = len))  # random integer series 
df1 = pd.DataFrame({ 'Kb' : s })/10   # one col dataFrame with random numbers
df['Kb'] = df1[ 'Kb']   # make a new column with data for df 

full_path_variable = os.path.join("/media/data1", "{:}X.txt".format(filename))  #remove the X after confirmation
df.to_csv(full_path_variable, sep=' ' , index=False)
print "done"