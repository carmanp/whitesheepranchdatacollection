#!/usr/bin/env python

import datetime as dt
import math
import time

# maps short easy to read names to long serial numbers.
nameMap = {
  'doorway':  '2134q3fad3',
  'hallway':  '234nvs23vs',
  'bathroom': 'dfgwe4234r',
}

# QUICK FUNCTION TO GENERATE SOME JUNK NUMBERS.
def generate_random_sensor_data():
  time = dt.datetime.now()
  d = {}
  d['time'] = time
  d['2134q3fad3'] = math.radians(time.second)
  d['234nvs23vs'] = math.sin(math.radians(time.second))
  d['dfgwe4234r'] = math.cos(math.radians(time.second))

  return d

# Open the file for writing (will over-write an existing) and add the header 
with open("logfile.txt", 'w') as f:
  f.write("{:<30s} {:<10s} {:<10s} {:<10s}\n".format(
      'time',
      'doorway',
      'hallway',
      'bathroom'
  ))


ctr = 0
while ctr < 5:
  ctr += 1
  print "Generating random data..."
  sd = generate_random_sensor_data()

  print "Writing (appending) to file..."
  with open("logfile.txt", 'a') as f:
    f.write("{:<30s} {:<0.8f} {:<0.8f} {:<0.8f}\n".format(
        sd['time'].strftime('%c'),
        sd[nameMap['doorway']],  # <-- Use the nameMap to lookup the serial number!
        sd[nameMap['hallway']],  #     serial number is returned, and used to lookup
        sd[nameMap['bathroom']]  #     data value in the sd dictionary!
    ))

  print "Resting..."
  time.sleep(1)

print "DONE."