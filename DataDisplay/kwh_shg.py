#!/usr/bin/env python

# revising to display KWH    

import os
import pandas as pd

from bokeh.plotting import figure, output_file, show, curdoc
from bokeh.layouts import gridplot, row, column, widgetbox, layout
from bokeh.models.widgets import TextInput, Button, Paragraph
from bokeh.models import ColumnDataSource
from bokeh.io import output_file, show

import getData


def  update():
    '''
    This function updates the plot. It can be called on initial setup
    and should be called whenever the user changes the start/end date inputs
    and then clicks the update button.

    The function grabs the values of the date input widgets and uses those
    values to go get data from the text files, using the getData.py module.

    Once the data is fetched from the text files, this function updates the 
    ColumnDataSource data structures, which are the special containers that
    are used to back the Bokeh plots.

    This function has no arguments. It relies on the global (to this module)
    variables and objects that are setup for the Bokeh plot(s).

    This function doesn't return anything.
    '''
    print('building dataFrame')

    # Get the widget values
    start = input_start_date.value
    end = input_end_date.value

    # build the DataFrame
    df = getData.buildFrame(start, end, column_list=cols)
    df.index = pd.DatetimeIndex(df['Date_Time'])

    print(df.iloc[0:3]) # print first 3 rows
    print('DataFrame complete')

    # Set the data in the ColumnDataSources (special objects that 
    # back the plots in bokeh)
    cds_skw.data = dict(x=df.index.values , y=df['Skw'].values)
    cds_hkw.data = dict(x=df.index.values , y=df['Hkw'].values)
    cds_gkw.data = dict(x=df.index.values , y=df['Gkw'].values)
    cds_net.data = dict(x=df.index.values , y=df['Net'].values)
    
    # Sum the KW values for each minute and convert to hours.
    KWHs = df['Skw'].sum() / 60
    KWHh = df['Hkw'].sum() / 60
    KWHg = df['Gkw'].sum() / 60
    KWHn = df['Net'].sum() / 60
    
    # update the paragraph printouts.
    para_solar_kwh.text = "  Solar KWH = {0:7,.1f}\n".format( KWHs) 
    para_house_kwh.text = "  House KWH = {0:7,.1f}\n".format( KWHh) 
    para_garage_kwh.text = " Garage KWH = {0:7,.1f}\n".format( KWHg)    
    para_net_kwh.text = "S-(H+G) Net = {0:7,.1f}\n".format( KWHn)

    print('update complete')

################################################################################
# SETUP PLOT
################################################################################

# create some widgets: Button, input box, output paragraaphs
update_button = Button(label="Update plot")
update_button.on_click(update)    

# could make these date inputs smart and look up available data
# (look in /media/Bosexy, find first file, etc)
input_start_date = TextInput(value='2022-03-01', title="Start Date: YYYY-MM-DD")
input_end_date   = TextInput(value='2022-04-01', title="End Date: YYYY-MM-DD")

para_solar_kwh  = Paragraph(text= '      Solar KWH')
para_house_kwh  = Paragraph(text= '      House KWH')
para_garage_kwh = Paragraph(text= '     Garage KWH')
para_net_kwh    = Paragraph(text= '   S -(H+G) KWH')

widgetbox1 = widgetbox(input_start_date, input_end_date, update_button) 
widgetbox2 = widgetbox(
    para_solar_kwh, para_house_kwh, para_garage_kwh, para_net_kwh
)

# setup the data
cols = ['Date_Time', 'Hkw', 'Gkw', 'Skw', 'Net']
df = pd.DataFrame (data=[] , columns=cols)
df.index = pd.DatetimeIndex(df['Date_Time'])

cds_skw = ColumnDataSource(data=dict(x=df.index, y=df['Skw'].values))
cds_hkw = ColumnDataSource(data=dict(x=df.index, y=df['Hkw'].values))
cds_gkw = ColumnDataSource(data=dict(x=df.index, y=df['Gkw'].values))
cds_net = ColumnDataSource(data=dict(x=df.index, y=df['Net'].values))

# build 4 figures with one line each
fig1 = figure(
  title="{}".format('Solar kw'), x_axis_type="datetime", y_axis_label="KW", y_range=(0, 10), width=900, height=125 )
fig1.line( 'x', 'y', source=cds_skw, line_width=1, line_color='blue')

fig2 = figure(
	title="{}".format('House kw'), x_axis_type="datetime", y_axis_label="KW",y_range=(0, 6), width=900, height=125, x_range=fig1.x_range 	)
fig2.line( 'x', 'y', source=cds_hkw, line_width=1, line_color='red')
		  
fig3 = figure(
	title="{}".format('Garage kw'), x_axis_type="datetime", y_axis_label="KW", y_range=(0, 6), width=900, height=125, x_range=fig1.x_range  )
fig3.line( 'x', 'y', source=cds_gkw, line_width=1, line_color='red')

fig4 = figure(
    title="{}".format('Solar - (House + Garage)'), x_axis_type="datetime", y_axis_label="KW", y_range=(- 10 , 10), width=900, height=125, x_range=fig1.x_range  )
fig4.line( 'x', 'y', source=cds_net, line_width=1, line_color='green')

# put it all in a layout
l = layout([ 
    gridplot([ [fig1] , [fig2], [fig3], [fig4] ]),
    [widgetbox1, widgetbox2],
])
print('setup done')


# You can run the update function once here, to set the plot with default
# initial values, or if you don't run it the plots show up empty on page load
# and then once the user hits the Update button, they show up.
#update()

curdoc().add_root(l)





