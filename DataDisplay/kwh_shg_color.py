#!/usr/bin/env python
#  revising to display KWH    
# revising  for bi-color Net
import pandas as pd
from bokeh.plotting import figure, output_file, show, curdoc
from bokeh.layouts import gridplot, row, column, widgetbox, layout
from bokeh.models.widgets import TextInput, Button, Paragraph, Dropdown, RangeSlider, Select
from bokeh.models import ColumnDataSource
from bokeh.io import output_file, show
import getDataCols
import os

print ("0")
# create some widgets: Button, input box, output paragraaphs
Calc = Button(label="Calculate - Be patient. 30 sec or so")
RateInput = TextInput( value="0.078" , title="Fall River rate $/KWH")
output1 = Paragraph(text= '      Solar KWH')
output2 = Paragraph(text= '      House KWH')
output3 = Paragraph(text= '     Garage KWH')
output4 = Paragraph(text= '    S -(H+G) KWH')
output5 = Paragraph()
output6 = Paragraph()

#lists for the selectors
m_menu = [("1" , "January"   ), ( "2" , "February"  ) , ( "3" , "March" ) , ( "4" , "April" ) , ( "5" , "May   " ), ( "6" , "June") , ("7" , "July" ) , (  "8" , "August") , ( "9"  , "September"), ( "10" , "October" ) , ("11" , "November" ) , ( "12" , "December") ]
y_menu = [ ("2017" , "2017" ) , ( "2018" , "2018")]
#create some selectors
month = Select(title = "Month:", value = "1", options=m_menu) 
year = Select(title = "Year:", value = "2018", options=y_menu)
day_range = RangeSlider( start = 1, end = 31,  value = (1,31) , step = 1 , title ="Day Range")
# bundle up the widgets
inputs1 = widgetbox( year , month , day_range  ) 
inputs2 = widgetbox(RateInput)  #separate box to allow for space below the RangeSlider not needed for solar
calc_Display = widgetbox( Calc , output1 , output2 , output3 , output4 , output5 )

cols = [ 'Date_Time' , 'Hkw' ,  'Gkw' , 'Skw' , 'Net' ]
df = pd.DataFrame ( data = [cols] , columns = cols)
df.iloc[0,0] = '2017-01-01T01:01:01'    # replace the Date_Time data with a Pandas DatetimeIndex format
df.index = pd.DatetimeIndex(df['Date_Time'])
Net_pos = df
Net_neg = df

# setup the data
source1 = ColumnDataSource(data=dict(x=df.index, y=df['Skw'].values))
source2 = ColumnDataSource(data=dict(x=df.index, y=df['Hkw'].values))
source3 = ColumnDataSource(data=dict(x=df.index, y=df['Gkw'].values))
source4pos = ColumnDataSource(data=dict(x=df.index, y=Net_pos['Net'].values))
source4neg = ColumnDataSource(data=dict(x=df.index, y=Net_neg['Net'].values)) 

#build 4 figures . 3 with one line each , the 4th 'Net'  has two colors - red neg, blk pos
fig1 = figure(
  title="{}".format('Solar kw'), x_axis_type="datetime", y_axis_label="KW", y_range=(0, 10), width=900, height=100 )
fig1.line( 'x', 'y', source=source1, line_width=1, line_color='blue')

fig2 = figure(
	title="{}".format('House kw'), x_axis_type="datetime", y_axis_label="KW",y_range=(0, 6), width=900, height=100, x_range=fig1.x_range 	)
fig2.line( 'x', 'y', source=source2, line_width=1, line_color='red')
		  
fig3 = figure(
	title="{}".format('Garage kw'), x_axis_type="datetime", y_axis_label="KW", y_range=(0, 6), width=900, height=100, x_range=fig1.x_range  )
fig3.line( 'x', 'y', source=source3, line_width=1, line_color='red')

fig4 = figure(
    title="{}".format('Solar - (House + Garage)'), x_axis_type="datetime", y_axis_label="KW", y_range=(- 10 , 10), width=900, height=120, x_range=fig1.x_range  )
net_pos = fig4.line( 'x', 'y', source=source4pos , line_width=1, line_color='black')
net_net = fig4.line( 'x', 'y', source=source4neg , line_width=1, line_color='red')


#put it all in a layout
l = layout(
    [gridplot([ [fig1] , [fig2], [fig3], [fig4] ]),   #experimental.  provides  3 rows , col 1  wide  Still need to synch the plots
    [ inputs1, calc_Display],                 #these are both widgetboxes.  1 row, cols 1 ,2
#    [ inputs2 ]                               #widgetbox 1 row, col 1
    ]
    )
print('setup done')

def load_data(year, month, first_day, last_day, columns):
    pass
    



# add a callback to the calculate button. Getting the data and doing the calculations should be in here.
def  update():
    print ('building dataFrame')
    first , last = day_range.value  # tuple of numbers
    #print first , last  , month.value , year.value
    df = getDataCols.buildFrame( str(year.value) , str(month.value) , first , last, cols )
    df.index = pd.DatetimeIndex(df['Date_Time'])
    print(df.iloc[ 1 : 3])
    print('DataFrame complete')
    print("Extracting data")
    source1.data = dict(x=df.index.values , y=df['Skw'].values)
    source2.data = dict(x=df.index.values , y=df['Hkw'].values)
    source3.data = dict(x=df.index.values , y=df['Gkw'].values)
 
    Net_pos = df.loc[df[ 'Net' ] >= 0 ]
    source4pos.data = dict(x=Net_pos.index.values , y=Net_pos['Net'].values)
 
    Net_neg = df.loc[df[ 'Net' ]  < 0 ]
    source4neg.data = dict(x=Net_neg.index.values , y=Net_neg['Net'].values)    
    
    
    KWHs = df['Skw'].sum() / 60   #sum the KWvalues for each minute and convert to hours
    KWHh = df['Hkw'].sum() / 60   #sum the KWvalues for each minute and convert to hours
    KWHg = df['Gkw'].sum() / 60   #sum the KWvalues for each minute and convert to hours
    KWHn = df['Net'].sum() / 60   #sum the KWvalues for each minute and convert to hours
    output1.text = "  Solar KWH = {0:7,.1f}\n".format( KWHs) 
    output2.text = "  House KWH = {0:7,.1f}\n".format( KWHh) 
    output3.text = " Garage KWH = {0:7,.1f}\n".format( KWHg) 	
    output4.text = "S-(H+G) Net = {0:7,.1f}\n".format( KWHn)
    output5.text = " "
    output6.text = " "
    print('update complete')

update()    # make this call once in the beginning to get set up

Calc.on_click(update)  ##this makes it all happen again 	

curdoc().add_root(l)





