#!/usr/bin/env python

import os
import datetime as dt
import pandas as pd

# The path on the pi to where the data files are stored.
DATA_PATH = '/media/Bosexy'

def build_fname(date):
	f = os.path.join(DATA_PATH, "{}.txt".format(date.strftime('%Y-%m-%d')))
	#/media/Bosexy/2022-03-23.txt
	return f

def buildFrame(start_date_str, end_date_str, column_list=('Date_Time', 'Ps', 'Pa', 'Kb')):
	'''
	This function reads data from the /media drive and 
	builds a data frame that can be used in the bokeh 
	plotting. 

	There is one file for each day and the files look like this:

	    $ head /media/Bosexy/2022-04-08.txt 
               Date_Time     T1     T2     T3     T4     T5     T6     T7     T8     T9    T10    T11    T12     Tt     Tm     Tb    Hkw    Gkw    Skw   Net    Pc    Pd    Pb    Ps    Pa    P-    Ht    Hb    Gt    Gb    Kb
        2022-04-08T00:00     66     65     64     65     66     66     67     67     68     63     64     69     74     49     49    0.2    0.1    0.7   0.4   OFF   OFF   OFF   OFF   OFF   OFF   OFF   OFF   OFF   OFF   0.0
        ....
        2022-04-08T00:08     66     65     64     65     66     66     67     66     68     63     64     69     73     49     49    0.2    0.1    0.7   0.3   OFF   OFF   OFF   OFF   OFF   OFF   OFF   OFF   OFF   OFF   0.0

  Parameters
  ----------
  start_date :  str, formatted date YYYY-MM-DD
    The starting date for which files to read.

  end_date : str, formatted date YYYY-MM-DD
    The ending date for which files to read.

  column_list : list of strings
    A list of strings, column names, to select from the 
    file. Defaults to ('Date_Time', 'Ps', 'Pa', 'Kb').
    Pass a different list to override.

	Returns
	-------
	data : pandas.DataFrame
	  The DataFrame will have a datetime index and have 
	  columns corresponding to the text file(s).
	''' 

	start_date = dt.datetime.fromisoformat(start_date_str)
	end_date = dt.datetime.fromisoformat(end_date_str)

	cur_date = start_date
	cur_fname = build_fname(cur_date)

	# Skip missing files
	while not (os.path.exists(cur_fname)):
		print(f"Skipping! Can't find file {cur_fname}")
		cur_date = cur_date + dt.timedelta(days=1)
		cur_fname = build_fname(cur_date)

	# Read first file we find into a pandas DataFrame object.
	print(f"Found file: {cur_fname}")
	df = pd.read_csv(cur_fname , delim_whitespace=True)

	final_data = df.loc[:,column_list]

	while cur_date < end_date:
		cur_date += dt.timedelta(days=1)
		cur_fname = build_fname(cur_date) 

		if (os.path.exists(cur_fname)) == True:
			print(f'Reading file: {cur_fname}')
			df = pd.read_csv(cur_fname , delim_whitespace=True)
			column_subset = df.loc[:,column_list]
			final_data = final_data.append(column_subset, ignore_index=True)
			#print(tail[0:1])
		else: 
			print(f"file not found: {cur_fname}")
		

	cleanup_on_off = { "Ps":  { "ON" : 1, "OFF" : 0} , "Pa":  { "ON" : 1, "OFF" : 0} }
	final_data.replace(cleanup_on_off, inplace=True)

	return final_data






