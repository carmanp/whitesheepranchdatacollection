#!/usr/bin/env python

#  revised 12/28/2020  . changes to date entry  and button labels
#  revised 3/2/2022      move to Python 3  and changed data location
#  revised 1/1/2023    general fixes, whitespace, etc.

import os
import calendar
import datetime

import pandas as pd

from bokeh.plotting import figure, output_file, show, curdoc
from bokeh.layouts import gridplot, row, column, widgetbox, layout
from bokeh.models.widgets import TextInput, Button, Paragraph, Dropdown, RangeSlider, Select
from bokeh.models import ColumnDataSource
from bokeh.io import output_file, show

import getData

def get_prev_month_first_day(current):
  '''Gets the first day of the previous month

  From here: https://stackoverflow.com/questions/38200581/subtract-a-month-from-a-date-in-python
  '''

  _first_day = current.replace(day=1)
  prev_month_lastday = _first_day - datetime.timedelta(days=1)
  return prev_month_lastday.replace(day=1)

def get_prev_month_last_day(current):
  '''Gets the last day of the previous month.'''
  _first_day = current.replace(day=1)
  prev_month_lastday = _first_day - datetime.timedelta(days=1)
  return prev_month_lastday

# create some widgets: Button, input boxs, output paragraaphs
Calc = Button(label="CLICK TO CALCULATE - Be patient. 30 sec or so" , button_type="primary")    # "primary" sets color = blue
RateInput = TextInput( value="0.08" , title="Fall River rate $/KWH")                             # other options avalable
thisDay = datetime.date.today()

start = get_prev_month_first_day(thisDay)
end = get_prev_month_last_day(thisDay)


StartDate = TextInput(value = start.isoformat() , title="Start Date: YYYY-MM-DD")   # .isoformat() returns   "YYYY-MM-DD" 
EndDate = TextInput(value= end.isoformat() , title="End Date: YYYY-MM-DD")

AptDmd = TextInput(value="0.5" , title="Apt Heat Demand  0.0 to 1")

output1 = Paragraph(text='Boiler KWH')
output2 = Paragraph(text='Apartment run time')
output3 = Paragraph(text='Shop run time')

output4 = Paragraph(text='Apartment Share of KWH')
output5 = Paragraph(text='Apartment $ = KWH x Rate')
output6 = Paragraph()


inputs1 = column( StartDate , EndDate , RateInput , Calc )
calc_Display = column( output1 , output2 , AptDmd , output3 , output5 , output4 )


# Set up some dummy empty data sources. This gives us something to 
# assign to the source variable of the lines in the figures.
# Empty for now...
source1 = ColumnDataSource(data=dict())
source2 = ColumnDataSource(data=dict())
source3 = ColumnDataSource(data=dict())

# Build 3 figures with one line each
fig1 = figure(
		title="{}".format('Ps'),
    x_axis_type="datetime",
    y_axis_label="state",
    y_range=(0, 1),
    width=900, height=125
)
fig1.line('x', 'y', source=source1, line_width=1, line_color='blue')

fig2 = figure(
	  title="{}".format('Pa'),
    x_axis_type="datetime",
    y_axis_label="state",
    y_range=(0, 1), 
    width=900, height=125, 
    x_range=fig1.x_range
)
fig2.line('x', 'y', source=source2, line_width=1, line_color='green')
		  
fig3 = figure(
	  title="{}".format('Kb'),
    x_axis_type="datetime",
    y_axis_label="KW",
    y_range=(0, 10),
    width=900, height=125,
    x_range=fig1.x_range
)
fig3.line( 'x', 'y', source=source3, line_width=1, line_color='red')


# put it all in a layout
l = layout(
	[
    gridplot([ [fig1] , [fig2], [fig3] ]),   
	  [inputs1, calc_Display],
	]
)
print ('setup done')


def update():
  '''
  Builds (or updates) DataFrames that will be used to plot
  data on the graphs.
  '''
  print ('begin update')
  df = getData.buildFrame( str(StartDate.value), str(EndDate.value))
  df.index = pd.DatetimeIndex(df['Date_Time'])
  source1.data = dict(x=df.index.values , y=df['Ps'].values)
  source2.data = dict(x=df.index.values , y=df['Pa'].values)
  source3.data = dict(x=df.index.values , y=df['Kb'].values)
  Kb = df['Kb'].sum() / 60   # sum the minutes and convert to hours
  # Pump run times. ON = 1 OFF = 0 
  Tpa = df['Pa'].sum()
  Tps = df['Ps'].sum()
  if (Tpa + Tps) != 0 :
    Ka =  Kb *   float(AptDmd.value)  * Tpa / ( float(AptDmd.value) * Tpa + Tps)
  else:
    Ka = Ks = 0
  rate = float(RateInput.value)
  output1.text = "Boiler KWH ={0:7.1f}\n".format(Kb, Tpa, Tps) 
  output2.text = "Apartment run time ={0:7.1f}\n".format( Tpa) 
  output3.text = "Shop run time ={0:7.1f}\n".format(Tps) 	
  output5.text = "Apartment $ = KWH x Rate = ${0:7.2f}\n".format( Ka*rate)
  output4.text = "Apartment Share of KWH ={0:7.1f}  <------ ENTER IN SPREADSHEET\n".format(Ka)
  output6.text = " "

  print ('update done')


# Assign callback functions to buttons.
Calc.on_click(update)  #this makes it all happen again  

# Call the update function to get data to put on the plot.
update()

# Display it all...
curdoc().add_root(l)


