#!/usr/bin/env python

import pandas as pd
from bokeh.plotting import figure, output_file, show, curdoc
from bokeh.layouts import gridplot

df = pd.read_csv('/media/data1/2017-test.txt', delim_whitespace=True)  #2017-10-99.txt

# Pc    Pd    Pb    Ps    Pa    P-    Ht    Hb    Gt    Gb  Replacd P1 ---P6 with these

# Replace ON  and OFF  in selected columns of df with 1 and 0. 
cleanup_on_off = {"Pc":  { "ON" : 1, "OFF" : 0} , "Pd":  { "ON" : 1, "OFF" : 0} ,"Pb":  { "ON" : 1, "OFF" : 0} , "Ps":  { "ON" : 1, "OFF" : 0} , "Pa":  { "ON" : 1, "OFF" : 0} , "P-":  { "ON" : 1, "OFF" : 0} , "Ht":  { "ON" : 1, "OFF" : 0} , "Hb":  { "ON" : 1, "OFF" : 0} , "Gt":  { "ON" : 1, "OFF" : 0} , "Gb":  { "ON" : 1, "OFF" : 0} }
df.replace(cleanup_on_off, inplace=True)


df.index = pd.DatetimeIndex(df['Date_Time'])

figs = []
for s in ['Pc', 'Pd', 'Pb' , 'Ps', 'Pa', 'P-' , 'Ht', 'Hb', 'Gt' , 'Gb' ]:

  fig = figure(
    title="{}".format(s),
    x_axis_type="datetime",
    y_axis_label="state",
    width=900,
    height=75
    
  )

  slines = fig.line(x=df.index.values, y=df[s].values, line_width=1, line_color='blue')
  figs.append(fig)

for t in ['T1', 'T2','T3','T4','T5','T6','T7','T8','T9','T10','T11','T12']:

  fig = figure(
    title="{}".format(t),
    x_axis_type="datetime",
    y_axis_label="temp",
    width=900,
    height=100,
    
  )

  tlines = fig.line(x=df.index.values, y=df[t].values, line_width=1, line_color='red')
  figs.append(fig)
 
 # make it work with or without the column Kb 

if pd.Series(df.columns).str.match('Kb').any():
   list =('Hkw', 'Gkw','Skw','Net' ,'Kb')
else:
   list = ['Hkw', 'Gkw','Skw','Net']            
    

for k in list :

  fig = figure(
    title="{}".format(k),
    x_axis_type="datetime",
    y_axis_label="KW",
    width=900,
    height=100,
  )
  
  klines = fig.line(x=df.index.values, y=df[k].values, line_width=1, line_color='green')
  figs.append(fig)

grid=gridplot(figs, ncols=1)

curdoc().add_root(grid)


#from IPython import embed; embed()




