#!/usr/bin/env python

import pandas as pd
from bokeh.plotting import figure, output_file, show, curdoc
from bokeh.layouts import gridplot, column, widgetbox
from bokeh.models.widgets import TextInput, Button, Paragraph, Dropdown
from bokeh.io import output_file, show
import os
import time
from time import strftime
import datetime as dt
from datetime import datetime, timedelta
import calendar
import numpy as np

def inc_day(file):
	date = datetime.strptime(file, "%Y-%m-%d")
	modified_date = date + timedelta(days=1)
	return datetime.strftime(modified_date, "%Y-%m-%d")    

#datafile format:   2017-mm-dd.txt    as a string.  want to increment the dd value thru the needed range
#start with a month and a year: eg 10  , 2017 . make a date string from this 

# Note: dropdowns return a string  can be used as an integer with int(string)
#  hardcode these now , from dropdowns later - when I figure out how. 
yr = '2017'  #from dropdown   
mm = '11' # from a dropdown  01 - 12
# dd = '09'# replaced with begin and end
begin = '16'   # from a dropdown  01 - 31 
end = 	'20'		# from a dropdown 01  - 31  
days_in_month =  calendar.monthrange(int(yr), int(mm))[1]  # returns number of days
if int(end) > days_in_month:
	end = str(days_in_month)
s = '-';
seq = (yr , mm , begin ); 
filename = s.join(seq)  #assemble the date part of the file name
#filename   '2017-01-01'  for example 
#print filename  
loop_start = int(begin) + 1  #    for  loop will start with the second file  
full_path_variable = os.path.join("/media/data1", "{:}.txt".format(filename)) # filename and path

while (os.path.exists(full_path_variable)) == False:
 	filename = inc_day(filename)
	full_path_variable = os.path.join("/media/data1", "{:}.txt".format(filename)) # filename and path
	loop_start += 1

df = pd.read_csv(full_path_variable , delim_whitespace=True)  #2017-10-99.txt
head = df.loc[:,['Date_Time' , 'Ps' , 'Pa' , 'Kb']]          # get selected data for day 1. 1440 lines
#print head[0:1]
#print filename	
for d in range(loop_start , (int(end) + 1)):
	filename = inc_day(filename)
	full_path_variable = os.path.join("/media/data1", "{:}.txt".format(filename))  #incremented file name
#	print full_path_variable
	if (os.path.exists(full_path_variable)) == True:
		df = pd.read_csv(full_path_variable , delim_whitespace=True) #read the day d file 
		tail = df.loc[:,['Date_Time' , 'Ps' , 'Pa' , 'Kb']]  #get selected data for day d.  1440 lines
		head = head.append(tail, ignore_index=True)  # append to the previous
#		print tail[0:1]  # just to see what is going on.  First data line of Frame 'tail'
	else: 
		print "file not found"

cleanup_on_off = { "Ps":  { "ON" : 1, "OFF" : 0} , "Pa":  { "ON" : 1, "OFF" : 0} }
head.replace(cleanup_on_off, inplace=True)

df = head.iloc[np.r_[0:2, -2:0]]
print df
	
# How about writing this to a file on Data1.  Then Page creator can get it	





