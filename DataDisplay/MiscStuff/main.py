#!/usr/bin/env python
'''
import  must be in same directory else full path is needed
passed data is all strings
'''
import os
import time
from time import strftime
import datetime as dt
from datetime import datetime, timedelta
import calendar
import numpy as np

import pandas as pd
from bokeh.plotting import figure, output_file, show, curdoc
from bokeh.layouts import gridplot, row, column, widgetbox, layout
from bokeh.models.widgets import TextInput, Button, Paragraph, Dropdown, RangeSlider, Select
from bokeh.io import output_file, show

import getData


#lists for the selectors
m_menu = [("1" , "January"   ), ( "2" , "February"  ) , ( "3" , "March" ) , ( "4" , "April" ) ,
		( "5" , "May   " ), ( "6" , "June") , ("7" , "July" ) , (  "8" , "August") ,
		( "9"  , "September"), ( "10" , "October" ) , ("11" , "November" ) , ( "12" , "December") ]
y_menu = [ ("2017" , "2017" ) , ( "2018" , "2018")]
#create some selectors
month = Select(title = "Month:", value = "11", options=m_menu) 
year = Select(title = "Year:", value = "2017", options=y_menu)
day_range = RangeSlider( start = 0, end = 31,  value = (17,20) , step = 1 , title ="Day Range")

print 'starting'
def function_to_call(attr, old, new):
	print month.value   # string
	print year.value  #string
	first , last = day_range.value  # numbers some with decimal pt so probably floats
	print first
	print last
	print first * last
	
month.on_change('value', function_to_call)
year.on_change('value', function_to_call)
day_range.on_change('value', function_to_call)

print day_range.value




# bundle up the widgets
inputs1 = widgetbox( year , month , day_range  ) 
l = layout( inputs1 )
'''
	[gridplot([ [fig1] , [fig2], [fig3] ]),   #  experimental.  provides  3 rows , col 1  wide  Still need to synch the plots
	[ inputs1, calc_Display],   #these are both widgetboxes.  1 row, cols 1 ,2
	[ inputs2 ]                    #widgetbox 1 row, col 1
	]
	)
'''
curdoc().add_root(l)





#def buildFrame(yr, mm, begin, end):
'''
yr = 2017
mm = 11
begin = 16
end = 17

print getData.buildFrame( str(yr) , str(mm) , str(begin), str(end) )
 
 '''