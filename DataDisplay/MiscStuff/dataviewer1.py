#!/usr/bin/env python

import pandas as pd
from bokeh.plotting import figure, output_file, show, curdoc
from bokeh.layouts import gridplot

df = pd.read_csv('2017-09-10.txt', delim_whitespace=True)
df.index = pd.DatetimeIndex(df['Date_Time'])

# Need to jump thru some hoops to get the data from on/off string ('OFF') to values
# like 0 and 1 that can be passed for the "tops" of the vertical bars...
# Hoops -  use replace   "ON"  :  1   "OFF"  :  0

cleanup_on_off = {"P1":  { "ON" : 1, "OFF" : 0} , "P2":  { "ON" : 1, "OFF" : 0} ,"P3":  { "ON" : 1, "OFF" : 0} , "P4":  { "ON" : 1, "OFF" : 0} , "P5":  { "ON" : 1, "OFF" : 0} , "P6":  { "ON" : 1, "OFF" : 0} , "Ht":  { "ON" : 1, "OFF" : 0} , "Hb":  { "ON" : 1, "OFF" : 0} , "Gt":  { "ON" : 1, "OFF" : 0} , "Gb":  { "ON" : 1, "OFF" : 0} }

df.replace(cleanup_on_off, inplace=True)

figs = []
for p in ['P1', 'P2', 'P3']:

  fig = figure(
    title="{}".format(p),
    x_axis_type="datetime",
    y_axis_label="state",
    width=900,
    height=100,
    
  )

  
  #pbars = fig.vbar(x=df.index.values, width=100, bottom=-0.5, top=df[p].values, fill_color='red', line_color='black')

  tlines = fig.line(x=df.index.values, y=df[p].values, line_width=1, line_color='blue')
  figs.append(fig)

for t in ['T1', 'T2', 'T3']:

  fig = figure(
    title="Temperature {}".format(t),
    x_axis_type="datetime",
    y_axis_label="temp",
    width=900,
    height=100,
    
  )

  tlines = fig.line(x=df.index.values, y=df[t].values, line_width=1, line_color='red')
  figs.append(fig)



grid=gridplot(figs, ncols=1)

curdoc().add_root(grid)
#show(grid)

#from IPython import embed; embed()




