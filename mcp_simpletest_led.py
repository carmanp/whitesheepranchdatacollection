# SPDX-FileCopyrightText: 2017 Tony DiCola for Adafruit Industries
#
# SPDX-License-Identifier: MIT
 
# Simple demo of reading and writing the digital I/O of the MCP2300xx as if
# they were native CircuitPython digital inputs/outputs.
# Author: Tony DiCola

#Adapted from the above by PTC   2022 Feb 18



import time
import board
import busio
import digitalio
from adafruit_mcp230xx.mcp23017 import MCP23017
i2c = busio.I2C(board.SCL, board.SDA)
mcp = MCP23017(i2c)
pin7 = mcp.get_pin(7)
pin7.switch_to_output(value=True)
while True:
       pin7.value = False
       print (pin7.value)
       time.sleep(0.5)
       pin7.value = True
       print (pin7.value)
       time.sleep(0.5)

