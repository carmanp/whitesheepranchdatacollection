#!/usr/bin/env python

# Hey dad here is a way I came up with that might make the formatting of
# major string easier...but is uses a bunch of fancy tricks. See what you think...


import random
import datetime as dt

def getTemp(x):
  '''Junk function to return junk numeric data'''
  return random.uniform(1,10)


def getState(x):
  '''Junk function to return junk string data'''
  states = 'X,hot,false,cold,ready,broke'.split(',')
  idx = int(random.randint(0,len(states)-1))
  return states[idx]

# Make a list of dictionaries so we can keep track of 
# the column name, the header format code and the data format code.
cols = [
  dict(colname="DateTime", hfmt=':>22s', dfmt=':>22s'),
  dict(colname="T1", hfmt=':>7', dfmt=':>7.1f'),
  dict(colname="T2", hfmt=':>7', dfmt=':>7.1f'),
  dict(colname="T3", hfmt=':>7', dfmt=':>7.1f'),
  dict(colname="T4", hfmt=':>7', dfmt=':>7.1f'),
  dict(colname="T5", hfmt=':>7', dfmt=':>7.1f'),
  dict(colname="Ht", hfmt=':>6', dfmt=':>6s'),
  dict(colname="Hb", hfmt=':>6', dfmt=':>6s'),
  dict(colname="Gt", hfmt=':>6', dfmt=':>6s'),
  dict(colname="Gb", hfmt=':>6', dfmt=':>6s'),
  dict(colname="Kb", hfmt=':>6', dfmt=':>6.1f'),
]

# we can then synthesize the header format string with something like
# this:, the result is like this:
# '{:>22s}{:>7}{:>7}{:>7}{:>7}{:>7}{:>6}{:>6}{:>6}{:>6}{:>6}'
hfmtstring = ''.join(['{'+i['hfmt']+'}' for i in cols])

# Then we can pull out the actual names from our datastructure, so we 
# a list that looks like this:
# ['DateTime', 'T1', 'T2', 'T3', 'T4', 'T5', 'Ht', 'Hb', 'Gt', 'Gb', 'Kb']
col_names = [c['colname'] for c in cols]

# and finally, we can unpack the list as use it as the arguments to the
# format function
h = hfmtstring.format(*col_names)

# Now we need an organized way to get all the actual data - 
# so we gather the actual numbers and stuff them back into our list
# that we started above
for col in cols:
  if col['colname'] in 'T1,T2,T3,T4,T5,T6,T7'.split(','):
    col['data'] = getTemp(col['colname'])

  elif col['colname'] in 'Ht,Hb,Gt,Gb'.split(','):
    col['data'] = getState(col['colname'])

  elif col['colname'] in 'Kb':
    col['data'] = 34.63

  elif col['colname'] == 'DateTime':
    col['data'] = dt.datetime.utcnow().strftime("%Y-%m-%dT%H:%M")

# Now follow the same pattern for the data string that we used for the 
# header string:
# make the format string, then gather the data, but this time, put it
# in a dictionary instead of a list and then we unpack the dict into
# keyword arguments using the double star operator
dfmtstring = ''.join(['{'+i['colname']+i['dfmt']+'}' for i in cols])
col_data = {c['colname']:c['data'] for c in cols} 
d = dfmtstring.format(**col_data)

print(h)
print(d)

