#!/usr/bin/env python
#Datalog_9   revise to use MCP23017  io expander
#Datalog_8   automatic file creation and naming
#Datalog7_1  successor  9-94-17
#Datalog7  successor to Datalog6  09-02-17
#support for another  ADS1115 ADC (16-bit) instance    (0x49)  for 
# will be used for KW calculations
import sys
import os
import time
from time import strftime
from w1thermsensor import W1ThermSensor, Sensor, Unit, SensorNotReadyError, NoSensorFoundError, ResetValueError
import datetime as dt

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
#import Adafruit_GPIO as GPIO
import RPi.GPIO as io
io.setmode(io.BCM)
from datetime import timedelta
import busio
import board
import digitalio
from adafruit_mcp230xx.mcp23017 import MCP23017
from sender import Sender         # email sender: Sender is a function in sender.py  in   whitesheepranchdatacollection
# Create the I2C bus
i2c = busio.I2C(board.SCL, board.SDA)

from adafruit_mcp230xx.mcp23017 import MCP23017





print ("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
print ("  NOTE: YOU MUST RUN THIS SCRIPT WITH SUDO   ")
print ("   SO THAT THE KERNEL MODULES ARE LOADED!    ")
print ("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

# Global debug setting, controlled by command line args
DEBUG_SETTING = 0

def debug_print(s):
	'''A logging function for optional printing based on global debug setting.'''
	if DEBUG_SETTING:
		print (s)
	else:
		pass

def print_usage():
	''' The help string'''
	print ("""
	Usage:

	    $ sudo /home/pi/datacol-env/bin/python Datalog_10.py [debug | -h | --help | H | HELP]
	
	-h, --help, H, help, HELP      Print this message and quit
	debug                          Run the program as normal, but include extra print statements
	                               in the output

	To use this program keep the following in mind:
	 * must be run with sudo
	 * must have sensors connected
	 * you can access the help with --help, -h, etc
	 * you must have the data collection python virtual environment activated --  obsolete 
	 * you can pass command line arguments such as debug or --help (to print this message and quit)
	 * the normal
	""")

# Take in and process the command line arguments.
args = sys.argv
if len(args) > 1:
	if any([x in args for x in ['-h', '--help', 'H', 'help', 'HELP']]):
		print_usage()
		sys.exit()
	if args[1] == 'debug':
		print ("%%%%% DEBUG MODE %%%%%%")
		DEBUG_SETTING = 1

adc = ADS.ADS1115(i2c)

chan0 = AnalogIn(adc, ADS.P0)
chan1 = AnalogIn(adc, ADS.P1)
chan2 = AnalogIn(adc, ADS.P2)
chan3 = AnalogIn(adc, ADS.P3)

#  chantag = "chan" + str(i)  #  chan0 , chan1,  etc 
# chantag = AnalogIn(adc, ADS.("P" + str(i)))#  chan0/P0 , chan1/P1 ,  etc
   
#import Adafruit_GPIO.MCP230xx as MCP

mcp = MCP23017(i2c)  # MCP23017

'''
'T1' : '000006adb86c' ,  'T2' : '000006ad55fb'   these are the installed sensors
'T1' : '000006ae1e97' ,  'T2' : '000006ae7869'   these are the bench sensors  
'''

nameMap = {'T1' : '000006adb86c' ,  'T2' : '000006ad55fb'   , 'T3' : '000006ae5401' , 'T4' : '000006ad5f71' , 'T5' : '000006ae78f7' , 'T6' : '000006ae204c' , 'T7' : '000006ad5a81' , 'T8' : '000006ae3027' , 'T9' : '000008781d51' , 'T10' : '0000087686e7' , 'T11' : '000008784169' , 'T12' : '0000087829d1' , 'Ttt' : '00000876547e' , 'Ttm' : '00000876d4a8' , 'Ttb' : '0000087623eb' }

offsetMap = {  'T1' :  -0.12 , 'T2' : 0.8 , 'T3' : 0.14 , 'T4' : 0.06 , 'T5' : 0.11 , 'T6' : 0.03 , 'T7' : -0.19 , 'T8' : -0.0 , 'T9' : -0.0 , 'T10' : -0.0 , 'T11' : -0.0 , 'T11' : -0.0 , 'T12' : -0.0 , 'Ttt' : -0.0 , 'Ttm' : -0.0, 'Ttb' : -0.0}


'''
for k, v in pumpMap.items():
	io.setup( pumpMap[k] , io.IN, pull_up_down = io.PUD_UP)
'''
kwList = [ 0 ] * 20    # used in getKW_boiler()
p3List = [ 0 ] * 5     # used in getP3State()
line_count = 0

#  setup for Digital IO  pins. Essential that this be done BEFORE any of the INPUT objects are used.

#pins for python 3 and RPiGPIO.  pass in the  number x
  
#pumpMap =  { 'Pc' : pin8 , 'Pd' : pin9 , 'Pb' : pin10 , 'Ps' : pin11 , 'Pa' : pin12 , 'P-' : pin13 }   #revise these to match MCP23017
pumpMap =  { 'Pc' : 8 , 'Pd' : 9 , 'Pb' : 10 , 'Ps' : 11 , 'Pa' : 12 , 'P-' : 13 } 
#replace pin numbers with pin objects  


for key in pumpMap:                           #  key is the pump name : 'Pc' ,'Pd' etc
    pumpPin = mcp.get_pin(pumpMap[key])               #  create an IO object pumpPin
    pumpPin.direction = digitalio.Direction.INPUT     #  set the properties
    pumpPin.pull = digitalio.Pull.UP
    pumpMap[key] = pumpPin              #  this line replaces the pin number with the pinOBJECT at that pin. 
                        
#   print ( key  , pumpPin  ,    pumpMap[key] )             #  pumpMap[key] returns the pin number for the above Pump.  pumpPin is the IO object               
#           'Pc'  0x764d4e80      0x764d4e80                # should be like this  
                                                            #to get the value :   pumpMap['Pc'].value   True / False or int(pumpMap['Pc'].value)  1 / 0

dhwMap  =  { 'Ht' : 14 , 'Hb' : 15 , 'Gt' : 0 , 'Gb' : 1 }   # 99 for future connections
#replace pin numbers with pin objects 

for key  in dhwMap:
    dhwPin = mcp.get_pin(dhwMap[key])
    dhwPin.direction = digitalio.Direction.INPUT
    dhwPin.pull = digitalio.Pull.UP 
    dhwMap[key] = dhwPin
    
#   print (key  , dhwPin , dhwMap[key]  ) 



def	getTemp(name):
#if name == 'T1':
    
  try:
    sensor = W1ThermSensor(sensor_type=Sensor.DS18B20, sensor_id=nameMap[name])
    T = sensor.get_temperature(Unit.DEGREES_F) + offsetMap[name]
#    print("Sensor %s has temperature %.2f" % (sensor.id, sensor.get_temperature()))
  except SensorNotReadyError:
#    print("Sensor %s is not ready to read"  % (sensor.id))
    T = 0
  except NoSensorFoundError:
#    print ("No Sensor %s Found")
    T = 0
  except ResetValueError:
#    print ("Reset Value Error"  % (sensor.id))
    T = 0
#	elif name == 'T2':

#		sensor = W1ThermSensor(W1ThermSensor.THERM_SENSOR_DS18B20, nameMap[name])
#		T = sensor.get_temperature(W1ThermSensor.DEGREES_F) + offsetMap[name]
#	else:
#		T=0
  return  T



#  KW of garage boiler. It modulates by duty cycling
#  Use the average of the last 20 readings( 1 sec interval)
def getKW_boiler(list_of_adc_readings):
	try:
	   a= 10 * (chan3.voltage)      # 4.096 / 32767    # chan3.voltage
	except OSError:
	   a = 0
	   print ("V3 read fail")
	if a < 0:
	   a = 0
	kw = a* 240 / 1000
#	print (a)
	retList = list_of_adc_readings
	retList.insert(0, kw)
	retList.pop()

	sum = 0
	for i, value in enumerate(retList):
		sum += value
	avg = sum / len(retList)
#	print (avg)	
	# alternate implementation
	#for index in range (len(retList)):
	#	sum = retList[index] + sum
	
	return avg, retList
	
def getPcState(list):
  retList = list
#	state = int(not(pumpMap['Pc']))  # input high = OFF. invert to return 0 for OFF
  state = int(not(pumpMap['Pc'].value))        #  state = int(not(pumpMap['Pc'].value))

  #print (state)
  retList.insert(0, state)
  retList.pop()
	#print retList
  sum = 0
  for index in range ( len(retList)): 
    sum = retList[index] + sum
  if sum == 0:
      state = 'OFF'
  else:
      state = 'ON'
  #print(state)
  return state , retList


def getState(pin):      # for use with MCP23017 .
	if pin.value  ==  0:		#pin is an object.  .value returns  True / False  0 /1
		state = 'ON'   
	else: 
		state = 'OFF'        #  
	#print(state)
	return state


	
#log7  add kw for house, garage and solar. also provide NET S - (H+G)
# 2/28/2022    channel.voltage   returns the actual scaled input voltage.  
def getKW(id):
#id:  h , g, s, or net  
	#read ADC channel  , CT  :   0-5 V = 0-50 amps
	#	amps = 10 * adc scaled voltage
	try:
	   V0 = chan0.voltage     #house
	except OSError:
	   V0 = 0
	   print ("V0 read fail")

	try:
	   V1 = chan1.voltage      #garage
	except OSError:
	   V1 = 0
	   print ("V1 read fail")

	try:
	   V2 = chan2.voltage      #solar
	except OSError:
	   V2 = 0
	   print ("V2 read fail")

	if id == 'h':
#		a = 10 * V0 * 4.096/32767 # max V divided by highest int (integer range)
		a = 10 * V0   #  changed 2/28/22   
		if a <= 0:
			a = 0
	#	debug_print("chan0.voltage={}   h amps ={:>7.1f}".format(V0, a))
		kw = a * 120 / 1000 #  calculate KW
	
	if id == 'g':
#		a = 10 * V1 * 4.096/32767
		a = 10 * V1   #  changed 2/28/22
		if a <= 0:
			a = 0
	#	debug_print("read_adc(1)={}   g amps ={:>7.1f}".format(V1, a))
		kw = a * 120 / 1000 #  calculate KW
	
	if id == 's':
#		a = 10 * V2 * 4.096/32767 
		a = 10 * V2   #  changed 2/28/22
    #revised for 0 - 50 transducer
		if a <= 0:
			a = 0			
	#	debug_print("read_adc(2)={}   s amps ={:>7.1f}".format(V2, a))
		kw = a * 240 / 1000 #  calculate KW

	if id == 'net':
		a = 10 * V0 
		if a <= 0:
			a = 0
		h = a * 120 / 1000 #  calculate KW
		
		a = 10 * V1 
		if a <= 0:
			a = 0	
		g = a * 120 / 1000 #  calculate KW	
		
		a = 10 * V2   #revised for 0 - 50 transducer 
		if a <= 0:
			a = 0
		s = a * 240 / 1000 #  calculate KW
 
		debug_print("h kw={0:>4.1f}    g kw ={1:>4.1f}    s kw ={2:>4.1f}".format(h, g, s))
		kw = s - (h + g) 

		
		debug_print("kw  ={0:>7.1f}".format(kw))
		debug_print("")

	return kw
		
"""
print "h kw ={0:>4.1f}\n".format(getKW('h'))
print "g kw ={0:>4.1f}\n".format(getKW('g'))
print "s kw ={0:>4.1f}\n".format(getKW('s'))
print "net kw ={0:>4.1f}\n".format(getKW('net'))

"""
"""
>>> from time import strftime
>>> strftime("%m/%d/%Y")
'09/05/2017'
>>> d = strftime("%m/%d/%Y")
>>> d
'09/05/2017'
this is how to get the date for a file name
"""

# Create header String
headerString = "{:>22s}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>7}{:>6}{:>6}{:>6}{:>6}{:>6}{:>6}{:>6}{:>6}{:>6}{:>6}{:>6}{:>6}\n".format(  'Date_Time' , 'T1' , 'T2' , 'T3' , 'T4' , 'T5' , 'T6' , 'T7' , 'T8' , 'T9' , 'T10' , 'T11' , 'T12' , 'Tt' , 'Tm' , 'Tb' , 'Hkw' , 'Gkw' , 'Skw' ,'Net' , 'Pc' , 'Pd' , 'Pb' , 'Ps' , 'Pa' , 'P-' , 'Ht' , 'Hb' , 'Gt' , 'Gb' , 'Kb' )
print (headerString)

	
timeOut1 = timeOut2 = timeOut3 = dt.datetime.utcnow() - timedelta( hours = 7 )  #  MST = UTC - 7 hrs

EmailFlag = False    # beginning state   == True when a daily file is ready to send

while True: 
	now = dt.datetime.utcnow() - timedelta( hours = 7 )     # MST = UTC - 7 hrs
	if timeOut1 < now:
		data = getKW_boiler(kwList)    # returns  (avg , kwList)
		kwAvg = data[0]    #use this in datalogging below
		#print round(kwAvg, 1) 
		kwList = data[1]
		timeOut1 = now + timedelta( seconds = 1)
		
	if timeOut2 < now:
		#print 'x'    error tracing
		data = getPcState(p3List) 
		PcState = data[0]  # 'ON' or 'OFF' for use below
#		print (data)
		p3List = data[1]  # use for next time around
		
		timeOut2 = now + timedelta(seconds = 0.2)
		
	if timeOut3 < now:
		aFile =  now.strftime("%Y-%m-%d")  # Year Month Day "%Y-%m-%d"
   
		full_path_variable = os.path.join("/media/Bosexy", "{:}.txt".format(aFile))   #new file name and path
		if (os.path.exists(full_path_variable)) == False:      # file will exist unless the day just changed. 
			EmailFlag = True                                     # file not exist -day changed, set EmailFlag
			with open(full_path_variable, 'w') as f:		         # open a new file for the new day
				f.write (headerString)		
		
		full_path_variable_backup = os.path.join("/media/BosexyVT", "{:}.txt".format(aFile))   #new file name and path [backup --> BosexyVT]
		if (os.path.exists(full_path_variable_backup)) == False:      # file will exist unless the day just changed. 
			with open(full_path_variable_backup, 'w') as f:		
				f.write (headerString )		   
		
#   build the path/file name for the file to be attached to email
		if EmailFlag == True:
			yesterday = now - timedelta( days = 1)
			aFile =  yesterday.strftime("%Y-%m-%d")  #yesterday's file name:  Year Month Day "%Y-%m-%d"
			F_P_V = os.path.join("/media/Bosexy","{:}.txt".format(aFile))  # path to yesterday's file
      
			# use  this at the end of the while loop      sender = sender.sender(F_P_V)
			#  also reset the flag there


		myTimeStamp = now              # don't know what this is / was for ??

#     change to IO   getState ( pin object)   will return ON  or OFF.    pumpMap['Pc'] is the INPUT object for Pump Pc

		dataString =  "{0:>22s}{1:>7.0f}{2:>7.0f}{3:>7.0f}{4:7.0f}{5:>7.0f}{6:>7.0f}{7:>7.0f}{8:>7.0f}{9:>7.0f}{10:>7.0f}{11:>7.0f}{12:>7.0f}{13:>7.0f}{14:>7.0f}{15:>7.0f}{16:>7.1f}{17:>7.1f}{18:>7.1f}{19:>6.1f}{20:>6s}{21:>6s}{22:>6s}{23:>6s}{24:>6s}{25:>6s}{26:>6s}{27:>6s}{28:>6s}{29:>6s}{30:>6.1f}\n".format(myTimeStamp.strftime("%Y-%m-%dT%H:%M") , getTemp('T1') , getTemp('T2') , getTemp('T3') , getTemp('T4') , getTemp('T5') , getTemp('T6') , getTemp('T7') , getTemp('T8'), getTemp('T9'), getTemp('T10'), getTemp('T11'), getTemp('T12'), getTemp('Ttt') , getTemp('Ttm') , getTemp('Ttb') , getKW("h") , getKW("g") , getKW("s") , getKW("net") , PcState , getState(pumpMap['Pd']) , getState(pumpMap['Pb']) , getState(pumpMap['Ps']) , getState(pumpMap['Pa']) , getState(pumpMap['P-']) , getState(dhwMap['Ht']) , getState(dhwMap['Hb']) , getState(dhwMap['Gt']) , getState(dhwMap['Gb']) , kwAvg )
		
		with open( full_path_variable, 'a' ) as f:
			f.write (dataString)

		with open( full_path_variable_backup, 'a' ) as f:        # duplicate to backup drive: BosexyVT
			f.write (dataString)

		print (dataString)
		timeOut3 = now + timedelta( seconds = 60)
		
		line_count  += 1
		if line_count == 10:
			print (headerString)
			line_count = 0

## beginning of Email Section
#   F_P_V  is the full path variable for yesterdays file
#### 
		if EmailFlag == True:
#			Sender( F_P_V )                        #     from sender  import Sender()   sender.py 
#			print ("Yesterday's file emailed")         No need - Sender prints messages
			EmailFlag = False                                      # reset the flag
      
 ####     
      
  #		  "this is the end , my friend "   The Doors,  late 60s






# datastring = 
